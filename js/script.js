let switchMode = document.getElementById("switchMode");
switchMode.onclick = function(){
   let theme = document.getElementById("theme");
   if(theme.getAttribute("href") == "./css/style-light-mode.css"){
      theme.href = "./css/style-dark-mode.css";
   } else {
      theme.href = "./css/style-light-mode.css";
   }
   // localStorage.setItem("userTheme", theme.href)
   // const userTheme = localStorage.getItem("userTheme");
}
